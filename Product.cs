﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp
{
    internal class Product
    {
        // fields 
        #region readonly variable
        //internal readonly string name="Mobile";
        //internal readonly int productId=1;
        //internal const int price = 100;
        #endregion
        internal  string name = "Mobile";
        internal  int productId = 1;
        internal  int price = 100;
        //constructor
        //ctor+2 times tab key
        public Product(string name, int productId, int price)
        {
            //name = "Laptop";
            //productId = 1;
            // this Contain the current instance 
            // here we can take the same variable name that's why we use this keyword.
            this.name = name;
            this.productId = productId;
            this.price = price;

        }
        public int GetConstValue()
        {
            return price;
        }
        internal Product GetProductDetails(Product product)
        {
            return product;
        }
    }
}
